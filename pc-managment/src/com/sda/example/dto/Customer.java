package com.sda.example.dto;

public class Customer {
  private String name;
  private String surname;
  private String email;
  private String celNumber;
  private String address;
  private Integer age;

  public Customer(String name, String surname, String email, String celNumber,
      String address, Integer age) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.celNumber = celNumber;
    this.address = address;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCelNumber() {
    return celNumber;
  }

  public void setCelNumber(String celNumber) {
    this.celNumber = celNumber;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "Customer{" +
        "name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", email='" + email + '\'' +
        ", celNumber='" + celNumber + '\'' +
        ", address='" + address + '\'' +
        ", age=" + age +
        '}';
  }
}
