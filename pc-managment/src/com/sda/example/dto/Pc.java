package com.sda.example.dto;
import java.util.Date;

public class Pc {
  private Type type;
  private Shop shop;
  private String model;
  private String isbn;
  private Date dateOfProduction;
  private double price;
  private Integer ram;
  private String os;
  private String processor;
  private Integer storage;

  public Pc(Type type, Shop shop, String model, String isbn, Date dateOfProduction, double price,
      Integer ram, String os, String processor, Integer storage) {
    this.type = type;
    this.shop = shop;
    this.model = model;
    this.isbn = isbn;
    this.dateOfProduction = dateOfProduction;
    this.price = price;
    this.ram = ram;
    this.os = os;
    this.processor = processor;
    this.storage = storage;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public Shop getShop() {
    return shop;
  }

  public void setShop(Shop shop) {
    this.shop = shop;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getIsbn() {
    return isbn;
  }

  public void setIsbn(String isbn) {
    this.isbn = isbn;
  }

  public Date getDateOfProduction() {
    return dateOfProduction;
  }

  public void setDateOfProduction(Date dateOfProduction) {
    this.dateOfProduction = dateOfProduction;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Integer getRam() {
    return ram;
  }

  public void setRam(Integer ram) {
    this.ram = ram;
  }

  public String getOs() {
    return os;
  }

  public void setOs(String os) {
    this.os = os;
  }

  public String getProcessor() {
    return processor;
  }

  public void setProcessor(String processor) {
    this.processor = processor;
  }

  public Integer getStorage() {
    return storage;
  }

  public void setStorage(Integer storage) {
    this.storage = storage;
  }

  @Override
  public String toString() {
    return "Pc{" +
        "type=" + type +
        ", shop=" + shop +
        ", model='" + model + '\'' +
        ", isbn='" + isbn + '\'' +
        ", dateOfProduction=" + dateOfProduction +
        ", price=" + price +
        ", ram=" + ram +
        ", os='" + os + '\'' +
        ", processor='" + processor + '\'' +
        ", storage=" + storage +
        '}';
  }
}
