package com.sda.example.dto;

import java.util.Date;
public class Order {

  private Customer customer;
  private Pc pc;
  private Date date;
  private Integer amount;
  private double payment;


  public Order(Customer customer, Pc pc, Date date, Integer amount, double payment) {
    this.customer = customer;
    this.pc = pc;
    this.date = date;
    this.amount = amount;
    this.payment = payment;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Pc getPc() {
    return pc;
  }

  public void setPc(Pc pc) {
    this.pc = pc;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public double getPayment() {
    return payment;
  }

  public void setPayment(double payment) {
    this.payment = payment;
  }

  @Override
  public String toString() {
    return "Order{" +
        "customer=" + customer +
        ", pc=" + pc +
        ", date=" + date +
        ", amount=" + amount +
        ", payment=" + payment +
        '}';
  }
}
