package com.sda.example.service;

import com.sda.example.dto.Pc;
import com.sda.example.dto.Type;
import java.util.Date;

public class PcService {

  public Pc populatePc() {
    ShopService shopService = new ShopService();

    return new Pc(Type.LAPTOP, shopService.populateShop(), "HP", "1234",
        new Date(), 1000, 16,
        "Windows 10", "i7 gen 9 hexa", 512);
  }

}
