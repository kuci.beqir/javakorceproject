package com.sda.example.service;

import com.sda.example.dto.Order;
import java.util.Date;

public class OrderService {

  public Order populateOrder() {
    CustomerService customerService = new CustomerService();
    PcService pcService = new PcService();

    return new Order(customerService.populateCustomer()
        , pcService.populatePc(), new Date(), 2, getPayment(pcService));

  }

  private double getPayment(PcService pcService) {
    return pcService.populatePc().getPrice() * 2;
  }

}
