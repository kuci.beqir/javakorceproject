package com.sda.example.service;

import com.sda.example.dto.Customer;

public class CustomerService {

  public Customer populateCustomer() {
    return new Customer("Taqo", "Maqo", "taqo@gmail.com",
        "0691123456", "Taqo from Korca", 30);
  }

}
